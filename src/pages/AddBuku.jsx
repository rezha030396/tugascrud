import React, { useState } from 'react';
import axios from 'axios';


function Book() {
  const [form, setState] = useState({
    title: '',
    author: '',
    published_date:'',
    pages: '',
    language:'',
    publisher_id:''
  });

  const printValues = e => {
    e.preventDefault();
  };

  const handleSubmit = (e) => {
    e.preventDefault()
    axios.post('http://127.0.0.1:3000/books',form );
}

  const updateField = e => {
    setState({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Title:
        <input
          value={form.title}
          name="title"
          onChange={updateField}
        />
      </label>
      <br />
      <label>
        Author:
        <input
          value={form.author}
          name="author"
          type="text"
          onChange={updateField}
        />
      </label>
      <br />
      <label>
        Published Date:
        <input
          value={form.published_date}
          name="published_date"
          type="date"
          onChange={updateField}
        />
      </label>
      <br />
      <label>
        Pages:
        <input
          value={form.pages}
          name="pages"
          onChange={updateField}
        />
      </label>
      <br />
      <label>
        language:
        <input
          value={form.language}
          name="language"
          onChange={updateField}
        />
      </label>
      <br />
      <label>
        Publisher Id:
        <input
          value={form.publisher_id}
          name="publisher_id"
          onChange={updateField}
        />
      </label>
      <br />
      <button>Submit</button>
    </form>
  );
}

export default Book
