import React from "react";
import axios from "axios";
import moment from "moment";
import "../App.css";
import { FormErrors } from "../validation/FormError";

class CRUDBook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      books: [],
      title: "",
      author: "",
      published_date: "",
      pages: "",
      language: "",
      publisher_id: "",
      formErrors: {
        title: "",
        author: "",
        published_date: "",
        pages: "",
        language: "",
        publisher_id: ""
      },
      titleValid: false,
      authorValid: false,
      published_datedaValid: false,
      pagesValid: false,
      languageValid: false,
      publisher_idValid: false,
      formValid: false,
      edit_mode: false
    };
  }

  handleUserInput = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };
  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let titleValid = this.state.titleValid;
    let authorValid = this.state.authorValid;
    let published_dateValid = this.state.published_datedaValid;
    let pagesValid = this.state.pagesValid;
    let languageValid = this.state.languageValid;
    let publisher_idValid = this.state.publisher_idValid;

    switch (fieldName) {
      case "title":
        titleValid = value.length != 0;
        titleValid = value.length >= 3;
        
        fieldValidationErrors.Woi = titleValid ? "" : "Harus Di isi Atau Terlalu Pendek";
        
        break;
      case "author":
        authorValid = value.length != 0;
        fieldValidationErrors.author = authorValid ? "" : "Harus Di isi";
        break;
      case "pages":
        pagesValid = value.length != 0;
        fieldValidationErrors.pages = pagesValid ? "" : "Harus Di isi";
        break;
        case "language":
        languageValid = value.length != 0;
        fieldValidationErrors.language = languageValid ? "" : "Harus Di isi";
        break;
        case "publisher_id":
          publisher_idValid = value.length != 0 
          publisher_idValid = value.length <= 5;
        fieldValidationErrors.publisher_id = publisher_idValid ? "" : "Harus Di isi atau terlalu panjang";
        break;
        

      default:
        break;
    }
    this.setState(
      {
        formErrors: fieldValidationErrors,
        titleValid: titleValid,
        authorValid: authorValid
      },
      this.validateForm
    );
  }
  validateForm() {
    this.setState({
      formValid: this.state.emailValid && this.state.passwordValid
    });
  }

  errorClass(error) {
    return error.length === 0 ? "" : "has-error";
  }

  componentDidMount() {
    axios.get("http://127.0.0.1:3000/books").then(res => {
      const books = res.data;
      this.setState({ books: books.data });
      console.log(books);
    });
  }

  handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    });

    this.handleUserInput(event);
  };

  handleSubmit = event => {
    event.preventDefault();
    const data = {
      title: this.state.title,
      author: this.state.author,
      published_date: this.state.published_date,
      pages: this.state.pages,
      language: this.state.language,
      publisher_id: this.state.publisher_id
    };

    console.log(data);
    axios
      .post(`http://127.0.0.1:3000/books`, data)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      })
      .finally(() => {
        this.componentDidMount();
        this.setState({
          id: "",
          title: "",
          author: "",
          published_date: "",
          pages: "",
          language: "",
          publisher_id: ""
        });
        console.log(this.state);
      });
  };

  handleDelete = id => {
    axios
      .delete(`http://127.0.0.1:3000/books/${id}`)
      .then(res => {
        console.log(res);
        console.log(res.data);
        alert(res.data.message);
      })
      .catch(err => {
        console.log(err);
      })
      .finally(() => {
        this.componentDidMount();
      });
  };

  handleEdit = id => {
    axios
      .get(`http://127.0.0.1:3000/books/${id}`)
      .then(res => {
        const temp = res.data.data[0].published_date;
        const published_date = moment(temp).format("YYYY-MM-DD");
        this.setState({
          id: res.data.data[0].id,
          title: res.data.data[0].title,
          author: res.data.data[0].author,
          published_date: published_date,
          pages: res.data.data[0].pages,
          language: res.data.data[0].language,
          publisher_id: res.data.data[0].publisher_id,
          edit_mode: true,
          formErrors: {
            title: "",
            author: "",
            published_date: "",
            pages: "",
            language: "",
            publisher_id: ""
          }
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleEditBook = event => {
    event.preventDefault();
    const id = this.state.id;
    const data = {
      title: this.state.title,
      author: this.state.author,
      publishedDate: this.state.published_date,
      pages: this.state.pages,
      language: this.state.language,
      publisher_id: this.state.publisher_id
    };
    axios
      .put(`http://127.0.0.1:3000/books/${id}`, data)
      .then(res => {
        if (!res.error) {
          this.setState({
            id: "",
            title: "",
            author: "",
            published_date: "",
            pages: "",
            language: "",
            publisher_id: ""
          });
        }
      })
      .catch(err => {
        console.log(err);
      })
      .finally(() => {
        this.componentDidMount();
        this.setState({
          edit_mode: false
        });
      });
  };

  render() {
    return (
      <div className="container mt-5">
        <FormErrors formErrors={this.state.formErrors} />
        <h1>Formulir Buku</h1>
        <div className="row">
          <div className="col">
            <form
              onSubmit={
                this.state.edit_mode ? this.handleEditBook : this.handleSubmit
              }
            >
              <div className="form-group">
                <label htmlFor="title" className="col-form-label pt-0">
                  Title
                </label>
                <div className="row">
                  <div className="col-md-5">
                    <input
                      className="form-control"
                      id="title"
                      name="title"
                      placeholder="title"
                      type="text"
                      onChange={(this.handleChange, this.handleUserInput)}
                      value={this.state.title}
                    />
                    <div></div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="author" className="col-form-label pt-0">
                  Author
                </label>
                <div className="row">
                  <div className="col-md-5">
                    <input
                      className="form-control"
                      id="author"
                      name="author"
                      placeholder="author"
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.author}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="published_date" className="col-form-label pt-0">
                  Published Date
                </label>
                <div className="row">
                  <div className="col-md-5">
                    <input
                      className="form-control"
                      id="published_date"
                      name="published_date"
                      placeholder="Published Date"
                      type="date"
                      onChange={this.handleChange}
                      // value={moment(this.state.published_date).format("YYYY-MM-DD")}
                      value={this.state.published_date}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="pages" className=" col-form-label pt-0">
                  Page
                </label>
                <div className="row">
                  <div className="col-md-5">
                    <input
                      className="form-control"
                      id="pages"
                      name="pages"
                      placeholder="Page"
                      type="number"
                      onChange={this.handleChange}
                      value={this.state.pages}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="language" className=" col-form-label pt-0">
                  Language
                </label>
                <div className="row">
                  <div className="col-md-5">
                    <input
                      className="form-control"
                      id="language"
                      name="language"
                      placeholder="Language"
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.language}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="publisher_id" className=" col-form-label pt-0">
                  Publisher Id
                </label>
                <div className="row">
                  <div className="col-md-5">
                    <input
                      className="form-control"
                      id="publisher_id"
                      name="publisher_id"
                      placeholder="Publisher Id"
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.publisher_id}
                    />
                  </div>
                </div>
              </div>
              <button className="btn btn-info" type="submit">
                {this.state.edit_mode ? "edit" : "submit"}
              </button>
            </form>
          </div>
          <div className="container mt-5">
            <table className="table table-responsive table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Published Date</th>
                  <th>Page</th>
                  <th>Language</th>
                  <th>Publisher Id</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {this.state.books.map(book => (
                  <tr key={book.id}>
                    <td>{book.id}</td>
                    <td>{book.title}</td>
                    <td>{book.author}</td>
                    <td>{moment(book.published_date).format("DD-MM-YYYY")}</td>
                    <td>{book.pages}</td>
                    <td>{book.language}</td>
                    <td>{book.publisher_id}</td>
                    <td>
                      <button
                        onClick={() => this.handleEdit(book.id)}
                        className="btn btn-info m-1"
                      >
                        Edit
                      </button>
                      <button
                        onClick={() => this.handleDelete(book.id)}
                        className="btn btn-info m-1"
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
export default CRUDBook;
