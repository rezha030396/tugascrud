import React from "react";
import axios from "axios";

export default class GetBook extends React.Component {
  state = {
    book: []
  };

  componentDidMount() {
    axios.get("http://localhost:3000/books").then(res => {
      const books = res.data;
      this.setState({ book: books.data});
      console.log(books.data);
    });
  }
  
  
  render() {
    // console.log(this.state.book);
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Author</th>
            </tr>
          </thead>
          <tbody>
            {this.state.book.map(book => (
              <tr key={book.id}>
                <td>{book.id}</td>
                <td>{book.title}</td>
                <td>{book.author}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}