import React from 'react';
import axios from 'axios';


class Home extends React.Component{
    constructor(){
        super()
        this.state ={
            users: [{
                name:"udin"
            }]
        }
    }
    componentWillMount(){
        const url ='http://jsonplaceholder.typicode.com/users'
        // const url = 'localhost:4000/bukus'
           axios.get(url)
           .then(users => {

          this.setState({
                users:users.data
            })
           })
        //    .then(data => {
        //        console.log(data)
        //    })
    }

    render(){
        console.log('------ini sedang di pasang')
        return(
            <div>
                {this.state.users.map((user,idx) => {
                    return(
                        <div>
                            {idx} : {user.name}
                        </div>
                    )
                })}
            </div>
        )
    }
    componentDidMount(){

    }
}
// const Home = (props) =>(
//     <div>
//         Welcome to my HomePage
//     </div>
// )

export default Home
