import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import App from './App';
import * as serviceWorker from './serviceWorker';
import "bootstrap/dist/css/bootstrap.min.css";
import Home from './pages/Home'
import Buku from './pages/Buku'
import AddBuku from './pages/AddBuku'
import Bukus from './pages/Bukus'

const routing = (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">
          Belajar API
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
  
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="navbar-brand" to="/pages/Home">
                Tampil Person
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/pages/Buku">
                Tampil Data Buku
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/pages/AddBuku">
                Tambah Data Buku
              </Link>
            </li>
            <li className="nav-item">
              <Link className="navbar-brand" to="/pages/Bukus">
                CRUD Data Buku
              </Link>
              </li>
          </ul>
        </div>
      </nav>
      <Route exact path="/" component={App} />
      <Route path="/pages/Home" component={Home} />
      <Route path="/pages/Buku" component={Buku} />
      <Route path="/pages/AddBuku" component={AddBuku} />
      <Route path="/pages/Bukus" component={Bukus} />
     
    </Router>
  );
  
ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
